import React, { Component } from 'react';
import Login from './containers/LoginComponent'
import Chat from './containers/ChatComponent'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import {Provider} from 'react-redux'
import { createStore, applyMiddleware } from "redux";
import rootReduces from './reducers';
import logger  from "redux-logger";
import AuthComponent from './containers/AuthComponent'
import { reactReduxFirebase } from 'react-redux-firebase'
import { compose } from 'redux'
import firebase from 'firebase'
import './firebase/index'
import handle from './firebase/handle'
import LoadingComponent from './containers/LoadingComponent'
const config = {
  userProfile: 'users'
}
const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, config)
)(createStore)
const store = createStoreWithFirebase(rootReduces,applyMiddleware(logger))

class App extends Component {
  componentDidMount(){
    handle()
  }
  render() {
    return (
      <Provider store={store}>
      <BrowserRouter>
       <LoadingComponent>
      <Switch>
         <Route exact path="/login" component={Login}/>
         <AuthComponent>
           <Route exact path="/" component={Chat}/>
         </AuthComponent> 
      </Switch>
      </LoadingComponent>
      </BrowserRouter>
     </Provider>
    );
  }
}

export default App;
