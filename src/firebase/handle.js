import firebase from 'firebase'
export default function handle() {
    firebase.auth().onAuthStateChanged(user => {
    const db = firebase.database();
    let Online = db.refFromURL('https://react-redux-firebase-a5e17.firebaseio.com/.info/connected');
    let userRef = db.refFromURL('https://react-redux-firebase-a5e17.firebaseio.com/presence/' + user.uid);
    
    Online.on('value', function (snapshot) {
      if (snapshot.val()) {
        userRef.onDisconnect().set({uid: user.uid, time: firebase.database.ServerValue.TIMESTAMP, online: false, displayName: user.displayName ? user.displayName : user.email.slice(0, user.email.indexOf('@')), photoURL: user.photoURL});
        userRef.set({uid: user.uid, time: firebase.database.ServerValue.TIMESTAMP, online: true, displayName: user.displayName ? user.displayName : user.email.slice(0, user.email.indexOf('@')), photoURL: user.photoURL});
      }
    });
  
    if(user){
      db.refFromURL(`https://react-redux-firebase-a5e17.firebaseio.com/users/${user.uid}/`).set({uid: user.uid, displayName: user.displayName ? user.displayName : user.email.slice(0, user.email.indexOf('@')), photoURL: user.photoURL, email: user.email})
    }
  })
  }