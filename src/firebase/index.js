import * as firebase from 'firebase'
const config = {
  apiKey: "AIzaSyAvIUWJLId83s9Bq7BhP1r0pelEZ19LeZA",
  authDomain: "react-redux-firebase-a5e17.firebaseapp.com",
  databaseURL: "https://react-redux-firebase-a5e17.firebaseio.com",
  projectId: "react-redux-firebase-a5e17",
  storageBucket: "react-redux-firebase-a5e17.appspot.com",
  messagingSenderId: "692684070996"
};
export default firebase.initializeApp(config);

export const database = firebase.database()
export const auth = firebase.auth();