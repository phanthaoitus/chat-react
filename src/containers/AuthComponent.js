
import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import {withRouter} from 'react-router-dom'
  class AuthComponent extends Component {
    componentDidUpdate() {
      const { auth } = this.props;
      if (isEmpty(auth) && isLoaded(auth)) {
        this.props.history.push('/login');
      }
    }
    componentWillMount() {
      const {auth} = this.props
      if (isEmpty(auth) && isLoaded(auth)) {
        this.props.history.push('/login');
      }
  }
    render() {
      const { auth,children } = this.props;
      return (!isEmpty(auth) && isLoaded(auth) ? <div>{children}</div>: null
      )}
  }
export default compose(
  withRouter,
  firebaseConnect(),
  connect(({ firebase: { auth } }) => ({ auth }))
)(AuthComponent)
