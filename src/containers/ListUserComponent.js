import { connect } from 'react-redux'
import { compose } from 'redux'
import { firebaseConnect } from 'react-redux-firebase'
import ListUser from '../components/ListUser';
function mapStateToProps(state) {
    return {
        user: state.firebase.data.presence,
        star: state.firebase.data.star
    };
}
export default compose (
    firebaseConnect((props) => [
        { path: 'presence'}, {path: 'star'}
      ]),
      connect(mapStateToProps)
)(ListUser)
  