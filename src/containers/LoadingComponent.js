import React, { Component } from 'react';
import Loading from '../components/Loading';
import { firebaseConnect, isLoaded } from 'react-redux-firebase'
import { compose } from 'redux'
import { connect } from 'react-redux'
import {withRouter} from 'react-router-dom'

class LoadingComponent extends Component {
  render() {
   const {auth,children}=this.props
   return (isLoaded(auth) ? <div>{children}</div>: <Loading/>)
}
}

export default compose(
    withRouter,
    firebaseConnect(),
    connect(({ firebase: { auth } }) => ({ auth }))
  )(LoadingComponent)
