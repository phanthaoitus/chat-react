
import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import Login from '../components/Login'

  class LoginComponent extends Component {
    componentWillMount() {
        const {auth} = this.props
        if (!isEmpty(auth) && isLoaded(auth)) {
          this.props.history.push('/');
        }
    }
      componentWillReceiveProps(nextProps) {
        if (!isEmpty(nextProps.auth) && isLoaded(nextProps.auth)) {
          nextProps.history.push('/');
        }
      }
    render() {
     return <Login {...this.props}/>
  }
}
export default compose(
  firebaseConnect(),
  connect(({ firebase: { auth } }) => ({ auth }))
)(LoginComponent)
