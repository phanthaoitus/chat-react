import {compose} from 'redux'
import {connect} from 'react-redux'
import {firebaseConnect} from 'react-redux-firebase'
import ChatComponent from '../components/Chat'
const mapStateToProps = state => {
    return {
        me: state.firebase.auth,
        listMessage: state.firebase.data.message,
    }
}
export default compose(
  firebaseConnect(props => [{path: 'message'}]),
    connect(mapStateToProps)
  )(ChatComponent);
