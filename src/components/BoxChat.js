import React from 'react';
import '../App.css'
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ImageUrl from'is-image-url'
import UploadImage from './UpImage'
import * as ReactDOM from 'react-dom';
class BoxChat extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      message: '',
      chanel: '',
      listMessage:[],
      star: false
    }
    this.sendClick = this.sendClick.bind(this)
    this.Star = this.Star.bind(this) 
  }
  getMessage(props){
    let list=[]
    if (props.receiver!==null){
    let chanel = props.me.uid < props.receiver.uid ? `${props.me.uid}:${props.receiver.uid}`: `${props.receiver.uid}:${props.me.uid}` 
    props.firebase.database().ref(`message/${chanel}`).on('value',(snapshoot) => {
      if(snapshoot.val()) {
          list = Object.keys(snapshoot.val()).map(item => snapshoot.val()[item]);
      }
    })
    props.firebase.database().ref(`star/${props.me.uid}/${props.receiver.uid}`).on('value',(snapshoot) => {
     this.setState({
       star: snapshoot.val()
     })
    })
    this.setState({
      chanel: chanel,
      listMessage: list,
    })}
  }
  Star() {
    let star = this.state.star
    this.props.firebase.database().ref(`star/${this.props.me.uid}/${this.props.receiver.uid}`).set(!star)
    this.setState({
      star: !star
    })
  }
  componentWillMount(){
    this.getMessage(this.props)
  }
  componentWillReceiveProps(props){
    this.getMessage(props)
  }
  scrollToBottom = () => {
    const { messageList } = this.refs;
    const scrollHeight = messageList.scrollHeight;
    const height = messageList.clientHeight;
    const maxScrollTop = scrollHeight - height;
    ReactDOM.findDOMNode(messageList).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  sendClick() {
    if (this.state.message !=='') {
      let sendMessage = {
        mess: this.state.message,
        chanel: this.state.chanel,
        from: this.props.me.uid,
        date: Date.now()
      }
      this.props.firebase.push(`message/${this.state.chanel}`,sendMessage)
    }
    this.setState({message:''})
  }
  render() {
    const {receiver,me} = this.props
      return (
      <div class="content">
		    <div class="contact-profile">
			    <img src={receiver.photoURL} alt={receiver.displayName} />
          <p>{receiver.displayName}</p>
          <i onClick={this.Star} class={this.state.star ? "fa fa-star star" : "fa fa-star"}></i>
        </div>
        <div class="messages" ref="messageList">
          <ul>
            {this.state.listMessage.map((item,index)=>(
              item.from !== me.uid ?
              <li key = {index}>
                <div class="message-data ">
                  <img src = {receiver.photoURL} alt ={receiver.displayName} />
                  <span class="message-data-time">{new Date(item.date).toLocaleString()}</span>
                </div>
                <div class="message my-message float-left">
                  {ImageUrl(item.mess) ? <img src={item.mess}/>: item.mess}
                </div>
              </li>
              : 
              <li class="clearfix">
              <div class="message-data align-right">
                <span class="message-data-time" >{new Date(item.date).toLocaleString()}</span> &nbsp; &nbsp;
                <img src={me.photoURL} class="float-right" alt ={me.displayName} />
              </div>
              <div class="message other-message float-right">
              {ImageUrl(item.mess) ? <img src={item.mess}/>: item.mess}
              </div>
            </li>
            ))}
          </ul>
        </div>
        <div class="message-input">
          <div class="wrap">
          <input type="text" placeholder="Write your message..." onChange= {(event) => this.setState({message:event.target.value})} value={this.state.message}/>
           <UploadImage chanel={this.state.chanel} from={this.props.me.uid}/>
          <button class="submit"  onClick={this.sendClick}><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
          </div>
        </div>
      </div>
    );
  }
}

export default BoxChat