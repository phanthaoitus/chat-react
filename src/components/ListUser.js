import React from 'react';
import '../App.css'

class ListUser extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      search: '',
      list: null,
    }
  }
  handleSearch = e => {
    this.setState({search: e.target.value})
  } 
  componentWillReceiveProps(props) {
    let list
    if(props.user) {
      list =  Object.keys(props.user).map(item => props.user[item])
      list = list.filter(item => item.uid !== props.me.uid)
      let listStar
      props.firebase.database().ref(`star/${props.me.uid}`).on('value',(snapshoot) => {
          listStar = snapshoot.val();
          if(listStar && listStar.length !=0) {
            list = list.map(item => ({...item,star: listStar[item.uid]}))
          }
      })
    }
    this.setState({
      list
    })
  }
  render() {
    const {onClick,me} = this.props
    if (this.state.list) sortList(this.state.list)
    return(
      <div id="sidepanel">
		<div id="profile">
			<div class="wrap">
				<img id="profile-img" src={me.photoURL} class="online" alt={me.displayName} />
				<p>{me.displayName}</p>
        <i class="fa fa-power-off expand-button" aria-hidden="true" onClick={()=>this.props.firebase.logout()}></i>
			</div>
		</div>
		<div id="search">
      <label><i class="fa fa-search" aria-hidden="true"></i></label>
			<input type="text" placeholder="Search contacts..." onChange={this.handleSearch} />
		</div>
		<div id="contacts">
			<ul>
        {this.state.list && searchList(this.state.list,this.state.search).map((item,index) =>(
          item.uid !== me.uid ?
            	<li class="contact" key={index}>
              <div class="wrap" onClick={() => onClick({uid: item.uid, displayName: item.displayName, photoURL:item.photoURL})}>
                <span class={item.online ? "contact-status online" : "contact-status offline"}></span>
                <img src={item.photoURL} alt={item.displayName} />
                <div class="meta">
                  <p class="name">{item.displayName}</p>
                  {!item.online ? <p class="preview">last online: {new Date(item.time).toLocaleString()}</p> : null}
                </div>
              </div>
            </li> : null
        ))} 
			</ul>
		</div>
  </div>
    )
  }
}
const sortList = (list) => {
  return list.sort(function(a,b){
    if (a.star && b.star) return  (b.time-a.time)
   return  !a.star && b.star ? 1 : -1
  })
}
const searchList = (list,search) =>{
  search = search.toUpperCase()
   return list.filter(item => {
     let username = item.displayName.toUpperCase()
     return username.includes(search)
   })
 }
export default ListUser