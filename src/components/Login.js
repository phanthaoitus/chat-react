import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css'

class Login extends Component {
  render(){
    return (
        <div className="container">
          <div className="login-form">
            <div className="main-div">
              <div className="panel">
                <h2>Login</h2>
                <hr></hr>
                <p>Please login with your google account to continue!</p>
              </div>
              <button type="button" className="btn btn-danger" onClick={()=>this.props.firebase.login({provider:'google',type:'popup'}).then(
                    res => this.props.history.push("/")
                )}><i className="fa fa-google"></i>   Login with google</button>
            </div>
          </div>
        </div>
    )
  }
}

export default Login