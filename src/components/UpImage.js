import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withHandlers, setPropTypes } from 'recompose';
import { firebaseConnect } from 'react-redux-firebase';
import Dropzone from 'react-dropzone';

const filesPath = 'uploadedFiles';
const handlers = {
  onFilesDrop: props => files => {
        files.map(file => {
          let upload = props.firebase.storage().ref(`${filesPath}/${file.name}`).put(file);
          upload.on('state_changed', function(snapshot){
          }, function(error) {
          }, function() {
            upload.snapshot.ref.getDownloadURL().then((url)=> {
              let sendMessage = {
                mess: url,
                chanel: props.chanel,
                from: props.from,
                date: Date.now()
              }
              props.firebase.push(`message/${props.chanel}`,sendMessage)
            });
          });
        })
      }
};

const enhancerPropsTypes = {
  firebase: PropTypes.object.isRequired
};
const enhance = compose(
  firebaseConnect([{ path: filesPath }]),
  connect(({ firebase: { data } }) => ({
    uploadedFiles: data[filesPath]
  })),
  setPropTypes(enhancerPropsTypes),
  withHandlers(handlers)
);

const Uploader = ({onFilesDrop }) => (
  <Dropzone onDrop={onFilesDrop} className="dropzone">
    <i class="fa fa-file-image-o attachment" aria-hidden="true"></i>
  </Dropzone>
);

Uploader.propTypes = {
  firebase: PropTypes.object.isRequired,
};

export default enhance(Uploader);