import React from 'react';
import ListUser from '../containers/ListUserComponent'
import BoxChat from './BoxChat'
import '../App.css'
class ChatComponent extends React.Component {
  state = {
    receiver: null
  };
  setReceiver = user => this.setState({receiver: user})
  render() {
    return (
      <div id="frame">
	      <ListUser onClick={this.setReceiver} me = {this.props.me} {...this.props}/>
        {this.state.receiver ? <BoxChat receiver={this.state.receiver} {...this.props}/>
          :
         <h2 className="text">Chat with everyone!</h2>
        }  
	    </div>
    );
  }
}
export default ChatComponent